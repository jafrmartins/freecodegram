<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $guarded = [];

    public function profileImage()
    {
        $imagePath = ($this->image) ? $this->image : 'profile/fpuScWXuAc9navBMdab64yYUxd0O3UP3gv4kJylt.png';
        return "/storage/" . $imagePath;
    }

    // Followers Relationship
    public function followers()
    {
        return $this->belongsToMany(User::class);
    }

    // User Relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
